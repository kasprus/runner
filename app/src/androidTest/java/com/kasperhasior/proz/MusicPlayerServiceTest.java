package com.kasperhasior.proz;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.os.Looper;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MusicPlayerServiceTest {
    @Rule
    public ServiceTestRule mServiceRule = new ServiceTestRule();
    private MusicPlayerService service;
    private MusicPlayerService.LocalBinder binder;
    @Before
    public void initialize() throws Exception {
        Intent serviceIntent = new Intent(InstrumentationRegistry.getTargetContext(), MusicPlayerService.class);
        try {
            binder = (MusicPlayerService.LocalBinder) mServiceRule.bindService(serviceIntent);
        }
        catch (TimeoutException e) {
            Log.e("error", "error");
            e.printStackTrace();
            throw new Exception();
        }
        service = binder.getService();
    }

    @Test
    public void musicHolderTest() throws Exception {
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                assertEquals(true, service.getSongTitles().isEmpty());
                assertEquals(true, service.getSongUris().isEmpty());
                List<String> ls = Arrays.asList(new String("a"), new String("b"), new String("c"));
                service.setSongTitles(ls);
                assertEquals(3, service.getSongTitles().size());
                assertEquals(true, service.getSongUris().isEmpty());
            }
        });
    }

    @Test
    public void musicPlayerExceptionTest() throws Exception {
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                service.play();
                service.stop();
                service.next();
                service.reset();
                service.previous();
                service.getProgress();
            }
        });
    }

}
