package com.kasperhasior.proz;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class TrainingServiceTest {
    @Rule
    public ServiceTestRule mServiceRule = new ServiceTestRule();
    private TrainingService service;
    private TrainingService.LocalBinder binder;
    @Before
    public void initialize() throws Exception {
        Intent serviceIntent = new Intent(InstrumentationRegistry.getTargetContext(), TrainingService.class);
        try {
            binder = (TrainingService.LocalBinder) mServiceRule.bindService(serviceIntent);
        }
        catch (TimeoutException e) {
            Log.e("error", "error");
            e.printStackTrace();
            throw new Exception();
        }
        service = binder.getService();
    }
    @Test
    public void isActive() throws Exception {
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                assertEquals(false, service.isActive());
                service.start();
                assertEquals(true, service.isActive());
                service.reset();
            }
        });
    }

    @Test
    public void multipleStarts() throws Exception {
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                assertEquals(false, service.isActive());
                service.start();
                service.start();
                service.start();
                assertEquals(true, service.isActive());
                service.reset();
            }
        });
    }

    @Test
    public void timeTest() throws Exception {
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                assertEquals(0, service.getTrainingTime());
                service.start();
                while(service.getTrainingTime() == 0) {
                }
                assertNotEquals(0, service.getTrainingTime());
                service.reset();
            }
        });
    }
}
