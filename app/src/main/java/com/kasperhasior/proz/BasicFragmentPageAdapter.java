package com.kasperhasior.proz;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/** Class providing implementation of page adapter for page view */
public class BasicFragmentPageAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragments;
    /** Creates object */
    public BasicFragmentPageAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    /** {@inheritDoc} */
    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    /** {@inheritDoc} */
    @Override
    public int getCount() {
        return this.fragments.size();
    }
}
