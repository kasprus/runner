package com.kasperhasior.proz;

/**Interface for observer pattern*/
public interface Observer {
    /**Updates state of the observer*/
    void update();
}
