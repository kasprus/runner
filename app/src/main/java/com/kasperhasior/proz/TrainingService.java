package com.kasperhasior.proz;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/** Class providing implementation of service for monitoring user activity */
public class TrainingService extends Service implements TrainingMonitor, Subject {
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private IBinder localBinder = new LocalBinder();
    private List<Observer> observerList = new ArrayList<>();
    private LocationManager locationManager;
    private LocationListener locationListener;
    private double totalDistance;
    private double lastDistance;
    private Location lastLocation;
    private double currentAltitude = 0;
    private double minimumAltitude = Double.MAX_VALUE;
    private double maximumAltitude = -Double.MAX_VALUE;
    private long prevTime = 0;
    private double currentSpeed = 0;
    private Timer timer;
    private boolean started = false;

    /** Creates object */
    public TrainingService() {
        super();
    }
    /** Class providing Binder enabling communication with service */
    public class LocalBinder extends Binder {
        /** Provides reference of the service */
        TrainingService getService() {
            return TrainingService.this;
        }
    }

    /** @inheritDoc */
    @Override
    public void onCreate() {
        super.onCreate();
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "TrainingService");
        locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if(!started)return;
                if(lastLocation != null) {
                    totalDistance += location.distanceTo(lastLocation);
                    lastDistance = location.distanceTo(lastLocation);
                    if(lastLocation == null) {
                        currentSpeed = 1000.0 * lastDistance / (System.currentTimeMillis() - prevTime);
                    }
                    else {
                        currentSpeed = 1000.0 * lastDistance / (double)(location.getTime() - lastLocation.getTime());
                    }
                } else {
                    currentSpeed = 0;
                }
                prevTime = System.currentTimeMillis();
                lastLocation = location;
                currentAltitude = location.getAltitude();
                maximumAltitude = Math.max(maximumAltitude, currentAltitude);
                minimumAltitude = Math.min(minimumAltitude, currentAltitude);
                sNotify();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        try {
            locationManager.requestLocationUpdates("gps", getResources().getInteger(R.integer.intervalGps), getResources().getInteger(R.integer.monitorMinimalDistance), locationListener);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
        secondsOfTraining = 0;
    }
    int secondsOfTraining;

    /** @inheritDoc */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }

    /** @inheritDoc */
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    /** @inheritDoc */
    @Override
    public void start() {
        if(!wakeLock.isHeld()) {
            wakeLock.acquire();
            NotificationCompat.Builder b = new NotificationCompat.Builder(this, "")
                    .setSmallIcon(R.drawable.ic_stat_training_notification)
                    .setContentTitle(getString(R.string.notificationTrainingServiceTitle))
                    .setContentText(getString(R.string.notificationTrainingServiceTitle))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            startForeground(getResources().getInteger(R.integer.trainingMonitorId), b.build());
        }
        timer = new Timer();
        final Handler handler = new Handler();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                ++secondsOfTraining;
                handler.post(()->{sNotify();});
            }
        };
        timer.scheduleAtFixedRate(task, 1000, 1000);
        started = true;
    }

    /** @inheritDoc */
    @Override
    public void stop() {
        if(wakeLock.isHeld()) {
            wakeLock.release();
            stopForeground(true);
        }
        started = false;
        if(timer != null)timer.cancel();
        lastLocation = null;
        lastDistance = 0;
        currentSpeed = 0;
        prevTime = 0;
        timer = null;
        sNotify();
    }

    /** @inheritDoc */
    @Override
    public double getDistance() {
        return totalDistance;
    }

    /** @inheritDoc */
    @Override
    public double getCurrentSpeed() {
        return currentSpeed;
    }

    /** @inheritDoc */
    @Override
    public double getAverageSpeed() {
        if(secondsOfTraining == 0)return 0;
        return totalDistance / secondsOfTraining;
    }

    /** @inheritDoc */
    @Override
    public int getTrainingTime() {
        return secondsOfTraining;
    }

    /** @inheritDoc */
    @Override
    public boolean isActive() {
        return started;
    }

    @Override
    public void register(Observer o) {
        if(observerList.contains(o))return;
        observerList.add(o);
        o.update();
    }

    /** @inheritDoc */
    @Override
    public void deregister(Observer o) {
        if(observerList.contains(o))observerList.remove(o);
    }

    /** @inheritDoc */
    @Override
    public void sNotify() {
        for(Observer o : observerList)o.update();
    }

    /** @inheritDoc */
    @Override
    public void reset() {
        if(isActive())stop();
        currentSpeed = 0;
        totalDistance = 0;
        lastLocation = null;
        secondsOfTraining = 0;
        lastDistance = 0;
        prevTime = 0;
        currentAltitude = 0;
        maximumAltitude = -Double.MAX_VALUE;
        minimumAltitude = Double.MAX_VALUE;
        sNotify();
    }

    /** @inheritDoc */
    @Override
    public double getCurrentAltitude() {
        return currentAltitude;
    }

    /** @inheritDoc */
    @Override
    public double getMinimumAltitude() {
        return minimumAltitude == Double.MAX_VALUE ? 0 : minimumAltitude;
    }

    /** @inheritDoc */
    @Override
    public double getMaximumAltitude() {
        return maximumAltitude == -Double.MAX_VALUE ? 0 : maximumAltitude;
    }
}
