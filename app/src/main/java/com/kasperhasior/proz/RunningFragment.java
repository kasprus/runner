package com.kasperhasior.proz;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by kasprus on 17.03.18.
 */

/**Class providing interaction between training monitor GUI and monitor logic*/
public class RunningFragment extends Fragment implements Observer {
    private FloatingActionButton startButton;
    private FloatingActionButton stopButton;
    private FloatingActionButton resetButton;
    private TextView distanceView;
    private TextView timeView;
    private TrainingService monitor;
    private TextView currentSpeed;
    private TextView averageSpeed;
    private TextView currentHeight;
    private TextView maximumHeight;
    private TextView minimumHeight;
    /**Class providing interaction between music player GUI and music player logic*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_runner, container, false);
        startButton = view.findViewById(R.id.startTrainingButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monitor = ((MainActivity)getActivity()).getMonitor();
                monitor.register(RunningFragment.this);
                if(!monitor.isActive())monitor.start();
            }
        });
        stopButton = view.findViewById(R.id.stopTrainingButton);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monitor = ((MainActivity)getActivity()).getMonitor();
                monitor.register(RunningFragment.this);
                if(monitor.isActive())monitor.stop();
            }
        });
        resetButton = view.findViewById(R.id.resetTrainingButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monitor = ((MainActivity)getActivity()).getMonitor();
                monitor.register(RunningFragment.this);
                monitor.reset();
            }
        });
        distanceView = view.findViewById(R.id.totalDistanceValue);
        timeView = view.findViewById(R.id.trainingTimeValue);
        currentSpeed = view.findViewById(R.id.currentSpeedValue);
        averageSpeed = view.findViewById(R.id.averageSpeedValue);
        currentHeight = view.findViewById(R.id.currentHeightValue);
        maximumHeight = view.findViewById(R.id.maximumHeightValue);
        minimumHeight = view.findViewById(R.id.minimumHeightValue);
        return view;
    }

    /**Class providing interaction between music player GUI and music player logic*/
    @Override
    public void onResume() {
        super.onResume();
        monitor = ((MainActivity)getActivity()).getMonitor();
        if(monitor != null)monitor.register(RunningFragment.this);

    }

    /**Class providing interaction between music player GUI and music player logic*/
    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity)getActivity()).getMonitor().deregister(RunningFragment.this);
    }

    /**Class providing interaction between music player GUI and music player logic*/
    @Override
    public void update() {
        distanceView.setText(formatDoubleText(monitor.getDistance()) + "m");
        timeView.setText(convertSecondsToMinutes(monitor.getTrainingTime()));
        averageSpeed.setText(formatDoubleText(convertToKMpH(monitor.getAverageSpeed())) + "km/h");
        currentSpeed.setText(formatDoubleText(convertToKMpH(monitor.getCurrentSpeed())) + "km/h");
        currentHeight.setText(formatDoubleText(monitor.getCurrentAltitude()) + "m");
        minimumHeight.setText(formatDoubleText(monitor.getMinimumAltitude()) + "m");
        maximumHeight.setText(formatDoubleText(monitor.getMaximumAltitude()) + "m");
    }

    private String convertSecondsToMinutes(int seconds) {
        return "" + (seconds / 60) + "m" + (seconds % 60) + "s";
    }

    private double convertToKMpH(double speed) {
        return speed * 3.6;
    }

    private String formatDoubleText(double speed) {
        long s = (long)(speed * 100);
        String ret = s / 100 + ".";
        s = s % 100;
        if(s < 10)ret += "0" + s;
        else {
            ret += s;
        }
        return ret;
    }
}
