package com.kasperhasior.proz;

import android.net.Uri;
import java.util.List;

/** Interface providing methods for storing and accessing sets of songs and their paths */
public interface MusicHolder {
    /**Returns list of song titles*/
    List<String> getSongTitles();
    /**Returns list of song URIs*/
    List<Uri> getSongUris();
    /**Sets list of song Uris*/
    void setSongsUris(List<Uri> songList);
    /**Sets list of song titles*/
    void setSongTitles(List<String> songList);
}
