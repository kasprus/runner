package com.kasperhasior.proz;

/**Interface implementing basic functionality of training monitor*/
public interface TrainingMonitor {
    /**Starts training*/
    void start();
    /**Stops training*/
    void stop();
    /**Returns spent distance*/
    double getDistance();
    /**Returns current speed*/
    double getCurrentSpeed();
    /**Returns average speed*/
    double getAverageSpeed();
    /**Returns current altitude*/
    double getCurrentAltitude();
    /**Returns minimal altitude*/
    double getMinimumAltitude();
    /**Returns maximal altitude*/
    double getMaximumAltitude();
    /**Returns total training time*/
    int getTrainingTime();
    /**Returns true if monitor has been started*/
    boolean isActive();
    /**Resets training monitor*/
    void reset();
}
