package com.kasperhasior.proz;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

/**Class implementing activity of settings*/
public class SettingsActivity extends AppCompatActivity {
    private Button saveAndExit;
    private Switch switchPlaylist;
    private SharedPreferences settingsPreferences;

    /** {@inheritDoc} */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        saveAndExit = (Button)findViewById(R.id.saveAndExitButton);
        switchPlaylist = (Switch)findViewById(R.id.playlistShuffle);
        saveAndExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        settingsPreferences = getSharedPreferences(getString(R.string.settingsPreferences), MODE_PRIVATE);
        boolean rp = settingsPreferences.getBoolean(getString(R.string.shufflePlaylist), getResources().getBoolean(R.bool.shufflePlaylist));
        switchPlaylist.setChecked(rp);
        switchPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsPreferences.edit().putBoolean(getString(R.string.shufflePlaylist), !settingsPreferences.getBoolean(getString(R.string.shufflePlaylist), getResources().getBoolean(R.bool.shufflePlaylist))).commit();
            }
        });
    }
}
