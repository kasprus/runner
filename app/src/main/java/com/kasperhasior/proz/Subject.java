package com.kasperhasior.proz;

/**Interface for observer pattern */
public interface Subject {
    /**Registers new observer*/
    public void register(Observer o);
    /**Deregisters selected observer*/
    public void deregister(Observer o);
    /**Notifies all observers about changes*/
    public void sNotify();
}

