package com.kasperhasior.proz;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;

/**Class providing interaction between music player GUI and music player logic*/
public class MusicPlayerFragment extends Fragment implements Observer {
    private Button selectMusic;
    private ImageButton playMusic;
    private ImageButton stopMusic;
    private ImageButton nextMusic;
    private ImageButton prevMusic;
    private ProgressBar musicProgress;
    private TextView songName;
    private List<String> selectedSongList = new ArrayList<>();
    private List<Long> selectedSongIds = new ArrayList<>();
    private List<String>listForAdapter = new ArrayList<>();
    private Timer timer = null;
    private TimerTask timerTask = null;
    private ArrayAdapter<String> adapter;
    private ListView mListView;

    /** @inheritDoc */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.music_player, container, false);
        mListView = view.findViewById(R.id.listOfSongs);
        songName = view.findViewById(R.id.songName);
        musicProgress = view.findViewById(R.id.musicProgressBar);
        musicProgress.setMax(getResources().getInteger(R.integer.maxProgressBar));
        selectMusic = view.findViewById(R.id.selectMusicButton);
        setOnClicListenerForMusicSelector();
        setPlayMusicButton(view);
        setStopMusicButton(view);
        setNextMusicButton(view);
        setPrevMusicButton(view);
        return view;
    }

    /** @inheritDoc */
    @Override
    public void onResume() {
        super.onResume();
        listForAdapter = new ArrayList<>();
        adapter=new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,
                listForAdapter);
        setListAdapter(adapter);
        if(((MainActivity)getActivity()).getMusicPlayerService() != null) {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    musicProgress.setProgress(((MainActivity)getActivity()).getMusicPlayerService().getProgress());
                }
            };
            timer.scheduleAtFixedRate(timerTask, 0, getResources().getInteger(R.integer.delayProgressBar));
            ((MainActivity)getActivity()).getMusicPlayerService().register(this);
            ((MainActivity)getActivity()).getMusicPlayerService().sNotify();
        }
    }

    /** @inheritDoc */
    @Override
    public void onPause() {
        super.onPause();
        if(timer != null)timer.cancel();
        if(timerTask != null)timerTask.cancel();
        timer = null;
        timerTask = null;
        ((MainActivity)getActivity()).getMusicPlayerService().deregister(this);
    }

    private List<String> getSongTitles() {
        return selectedSongList;
    }

    private List<Uri> getSongUris() {
        List<Uri> ret = new ArrayList<>();
        for(Long id : selectedSongIds) {
            ret.add(ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id));
        }
        return ret;
    }

    private void setPlayMusicButton(View view) {
        playMusic = view.findViewById(R.id.playButton);
        playMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(timer == null && timerTask == null) {
                    timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            musicProgress.setProgress(((MainActivity)getActivity()).getMusicPlayerService().getProgress());
                        }
                    };
                    timer = new Timer();
                    timer.scheduleAtFixedRate(timerTask, 0, getResources().getInteger(R.integer.delayProgressBar));
                }
                ((MainActivity)getActivity()).getMusicPlayerService().register(MusicPlayerFragment.this);
                ((MainActivity)getActivity()).getMusicPlayerService().play();
            }
        });
    }

    private void setStopMusicButton(View view) {
        stopMusic = view.findViewById(R.id.stopButton);
        stopMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).getMusicPlayerService().register(MusicPlayerFragment.this);
                ((MainActivity)getActivity()).getMusicPlayerService().stop();
            }
        });
    }

    private  void setNextMusicButton(View view) {
        nextMusic = view.findViewById(R.id.nextButton);
        nextMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).getMusicPlayerService().register(MusicPlayerFragment.this);
                ((MainActivity)getActivity()).getMusicPlayerService().next();
            }
        });
    }

    private void setPrevMusicButton(View view) {
        prevMusic = view.findViewById(R.id.previousButton);
        prevMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).getMusicPlayerService().register(MusicPlayerFragment.this);
                ((MainActivity)getActivity()).getMusicPlayerService().previous();
            }
        });
    }

    /** @inheritDoc */
    @Override
    public void update() {
        songName.setText(((MainActivity)getActivity()).getMusicPlayerService().getSongName());
        if(!adapter.isEmpty())adapter.clear();
        if(((MainActivity)getActivity()).getMusicPlayerService().getSongTitles() != null) {
            adapter.addAll(((MainActivity)getActivity()).getMusicPlayerService().getSongTitles());
        }
        mListView.setVisibility(getView().VISIBLE);
    }

    private void setOnClicListenerForMusicSelector() {
        selectMusic.setOnClickListener(new MusicSelectorListener());
    }

    private void shuffle(List<String> first, List<Long>second) {
         class Pair {
            public Pair(String a, Long b) {
                s = a;
                u = b;
            }
            public String s;
            public Long u;
        }
        List<Pair> pairs = new ArrayList<>();
        for(int i = 0; i < first.size(); ++i) {
            pairs.add(new Pair(first.get(i), second.get(i)));
        }
        Collections.shuffle(pairs);
        first.clear();
        second.clear();
        for(Pair p : pairs) {
            first.add(p.s);
            second.add(p.u);
        }
    }

    private ListView getListView() {
        if (mListView == null) {
            mListView = getView().findViewById(R.id.listOfSongs);
        }
        return mListView;
    }

    private void setListAdapter(ListAdapter adapter) {
        getListView().setAdapter(adapter);
    }

    /**Nested class which implements OnClickListener for music selector dialog*/
    private class MusicSelectorListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            final List<String> tmpSongList = new ArrayList<>();
            final List<Long> tmpSongIds = new ArrayList<>();
            final List<Long> tmpSelectedSongIds = new ArrayList<>();
            final List<String> tmpSelectedSongList = new ArrayList<>();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            ContentResolver musicResolver = getActivity().getContentResolver();
            Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
            if(musicCursor != null && musicCursor.moveToFirst()) {
                int titleNumber = musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
                int id = musicCursor.getColumnIndex(MediaStore.Audio.Media._ID);
                while(true) {
                    tmpSongList.add(musicCursor.getString(titleNumber));
                    tmpSongIds.add(musicCursor.getLong(id));
                    if(!musicCursor.moveToNext())break;
                }
            }
            final CharSequence[] displayArray = new CharSequence[tmpSongList.size()];
            for(int i = 0; i < tmpSongList.size(); ++i) {
                displayArray[i] = tmpSongList.get(i);
            }
            builder.setTitle(getString(R.string.selectMusic)).setMultiChoiceItems(displayArray, null, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                    if(b) {
                        tmpSelectedSongIds.add(tmpSongIds.get(i));
                        tmpSelectedSongList.add(displayArray[i].toString());
                    } else {
                        tmpSelectedSongList.remove(displayArray[i].toString());
                        tmpSelectedSongIds.remove(tmpSongIds.get(i));
                    }
                }
            });
            builder.setNeutralButton(R.string.sReturn, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ((MainActivity)getActivity()).getMusicPlayerService().reset();
                    selectedSongIds.clear();
                    selectedSongList.clear();
                    selectedSongList.addAll(tmpSelectedSongList);
                    selectedSongIds.addAll(tmpSelectedSongIds);
                    SharedPreferences sp = getActivity().getSharedPreferences(getString(R.string.settingsPreferences), MODE_PRIVATE);
                    if(sp.getBoolean(getString(R.string.shufflePlaylist), false)) {
                        shuffle(selectedSongList, selectedSongIds);
                    }
                    adapter.clear();
                    adapter.addAll(MusicPlayerFragment.this.getSongTitles());
                    ((MainActivity)getActivity()).getMusicPlayerService().setSongsUris(MusicPlayerFragment.this.getSongUris());
                    ((MainActivity)getActivity()).getMusicPlayerService().setSongTitles(MusicPlayerFragment.this.getSongTitles());
                    ((MainActivity)getActivity()).getMusicPlayerService().register(MusicPlayerFragment.this);
                }
            });
            builder.create().show();
        }
    }

}
