package com.kasperhasior.proz;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**Class providing service of music player */
public class MusicPlayerService extends Service implements MusicHolder, Subject, MusicPlayer {
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private IBinder localBinder = new LocalBinder();
    private List<Uri>songList = new ArrayList<>();
    private List<String>songTitles = new ArrayList<>();
    private List<Observer>observerList = new ArrayList<>();
    private int oldNumber = 0;
    private MediaPlayer player;
    private boolean paused = false;
    private boolean nextSong = false;
    public class LocalBinder extends Binder {
        MusicPlayerService getService() {
            return MusicPlayerService.this;
        }
    }

    /**Creates object*/
    public MusicPlayerService() {
        super();
    }

    /** {@inheritDoc} */
    @Override
    public void onCreate() {
        super.onCreate();
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MusicPlayerService");
        player = new MediaPlayer();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if(!nextSong)return;
                next();
            }
        });
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                nextSong = true;
                player.start();
            }
        });
    }

    /** {@inheritDoc} */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }

    /** {@inheritDoc} */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getSongTitles() {
        return songTitles;
    }

    /** {@inheritDoc} */
    @Override
    public List<Uri> getSongUris() {
        return songList;
    }

    /** {@inheritDoc} */
    @Override
    public void setSongsUris(List<Uri>songList) {
        this.songList.clear();
        paused = false;
        oldNumber = 0;
        this.songList.addAll(songList);
        sNotify();
    }

    /** {@inheritDoc} */
    @Override
    public void setSongTitles(List<String> songList) {
        songTitles.clear();
        songTitles.addAll(songList);
        sNotify();
    }

    /** {@inheritDoc} */
    @Override
    public void play() {
        sNotify();
        if(songList.isEmpty() || oldNumber >= songList.size())return;
            if(paused == true) {
                setLock();
                player.start();
                paused = false;
                return;
            }
            try {
                setLock();
                nextSong = false;
                player.reset();
                player.setDataSource(getApplicationContext(), songList.get(oldNumber));
                player.prepareAsync();
            } catch (IOException e) {
                resetLock();
                oldNumber = 0;
                player.reset();
                songList.clear();
                songTitles.clear();
            }
    }

    /** {@inheritDoc} */
    @Override
    public void stop() {
        resetLock();
        if(player.isPlaying()) {
            player.pause();
            paused = true;
        }
    }

    /** {@inheritDoc} */
    @Override
    public void next() {
        if(songList.isEmpty() || oldNumber >= songList.size() - 1) {
            resetLock();
            return;
        }
        ++oldNumber;
        paused = false;
        play();
        sNotify();
    }

    /** {@inheritDoc} */
    @Override
    public void previous() {
        if(songList.isEmpty() || oldNumber == 0) {
            resetLock();
            return;
        }
        --oldNumber;
        paused = false;
        play();
        sNotify();
    }

    /** {@inheritDoc} */
    @Override
    public void reset() {
        player.reset();
    }

    /** {@inheritDoc} */
    @Override
    public String getSongName() {
        if(oldNumber >= 0 && oldNumber < songTitles.size()) {
            return songTitles.get(oldNumber);
        }
        return getString(R.string.songName);
    }

    /** {@inheritDoc} */
    @Override
    public int getProgress() {
        int dur = player.getDuration();
        if(dur <= 0)return 0;
        int pos  = player.getCurrentPosition();
        pos *= getResources().getInteger(R.integer.maxProgressBar);
        return pos / dur;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isActive() {
        return player.isPlaying();
    }

    /** {@inheritDoc} */
    @Override
    public void register(Observer o) {
        if(observerList.contains(o))return;
        observerList.add(o);
        o.update();
    }

    /** {@inheritDoc} */
    @Override
    public void deregister(Observer o) {
        if(observerList.contains(o))observerList.remove(o);
    }

    /** {@inheritDoc} */
    @Override
    public void sNotify() {
        for(Observer o : observerList)o.update();
    }

    private void setLock() {
        if(!wakeLock.isHeld()) {
            wakeLock.acquire();
            NotificationCompat.Builder b = new NotificationCompat.Builder(this, "")
                    .setSmallIcon(R.drawable.ic_stat_music_player_notification)
                    .setContentTitle(getString(R.string.notificationMusicServiceTitle))
                    .setContentText(getString(R.string.notificationMusicServiceTitle))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            startForeground(getResources().getInteger(R.integer.musicPlayerId), b.build());
        }
    }

    private void resetLock() {
        if(wakeLock.isHeld()) {
            wakeLock.release();
            stopForeground(true);
        }
    }

}
