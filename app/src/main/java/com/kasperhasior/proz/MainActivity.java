package com.kasperhasior.proz;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

/** Basic class of the application */
public class MainActivity extends AppCompatActivity {

    private BasicFragmentPageAdapter fragmentAdapter = null;
    private MusicPlayerService player = null;
    private TrainingService monitor = null;

    /** Creates object */
    public MusicPlayerService getMusicPlayerService() {
        return player;
    }

    /** Returns reference of the object of TrainingService
     * @see TrainingService */
    public TrainingService getMonitor() {
        return monitor;
    }

    private ServiceConnection musicPlayerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MusicPlayerService.LocalBinder binder = (MusicPlayerService.LocalBinder)iBinder;
            player = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            player = null;
        }
    };

    private ServiceConnection trainingMonitorConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            TrainingService.LocalBinder binder = (TrainingService.LocalBinder)iBinder;
            monitor = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            monitor = null;
        }
    };

    /** {@inheritDoc} */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** {@inheritDoc} */
    @Override
    protected void onStart() {
        super.onStart();

        Intent musicPlayerIntent = new Intent(this, MusicPlayerService.class);
        startService(musicPlayerIntent);
        bindService(musicPlayerIntent, musicPlayerConnection, Context.BIND_AUTO_CREATE);
        Intent trainingServiceIntent = new Intent(this, TrainingService.class);
        startService(trainingServiceIntent);
        bindService(trainingServiceIntent, trainingMonitorConnection, Context.BIND_AUTO_CREATE);

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new RunningFragment());
        fragmentList.add(new MusicPlayerFragment());
        fragmentAdapter = new BasicFragmentPageAdapter(getSupportFragmentManager(), fragmentList);
        ((ViewPager)findViewById(R.id.fragmentPager)).setAdapter(fragmentAdapter);
    }

    /** {@inheritDoc} */
    @Override
    protected void onStop() {
        super.onStop();
    }

    /** {@inheritDoc} */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(musicPlayerConnection);
        unbindService(trainingMonitorConnection);
    }

    /** {@inheritDoc} */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.about:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.about);
                builder.setView(R.layout.about);
                builder.setNeutralButton(R.string.sReturn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.create().show();
                return true;
            case R.id.settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
