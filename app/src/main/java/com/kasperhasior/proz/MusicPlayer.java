package com.kasperhasior.proz;

/**Interface providing basic functionality of music player*/
public interface MusicPlayer {
    /**Starts playing a current song*/
    void play();
    /**Stops playing*/
    void stop();
    /**Resets music player*/
    void reset();
    /**Goes to next song*/
    void next();
    /**Goes to previous song*/
    void previous();
    /**Returns current song name*/
    String getSongName();
    /**Returns actual progress of playing a song*/
    int getProgress();
    /**Returns true if player is playing */
    boolean isActive();
}
